Mollom Stats
------------

Project Page:
http://drupal.org/project/molstats

By Garrett Albright
http://drupal.org/user/191212
Email: albright (at) anre [dot] net
AIM: AlbrightGuy - ICQ: 150670553 - IRC: Albright

Installation & Configuration
----------------------------

(better documentation coming eventually)

1. Install and configure Mollom, if you haven't already. Mollom Stats is pretty
useless if your site isn't using Mollom in the first place.

2. Install Mollom Stats as you would any other module.

3. Go to Administration > Site configuration > Blocks and move the Mollom Stats
block into the region where you would like it to appear. Click the "Save blocks"
button at the bottom. Mollom Stats should fetch stats from Mollom's server and
appear in the region in which you placed it.

OH NOES! THE BLOCK DIDN'T APPEAR! Things to check:

  1. Has Mollom been properly configured?
  
  2. Are Mollom's servers down?
  
Both of these can be confirmed by going to the page at Administer > Site 
configuration > Mollom. If Mollom's statistics graph is not working correctly,
Mollom Stats probably won't work either.

4. If you wish to customize the message shown in the block, click on Mollom
Stats' "configure" link on the block list page. Note that you will be instructed
to install the Token module if you have not already. (Mollom Stats will work
just fine without Token; however, you won't be able to customize the message.)

OH NOES! MOLLOM STATS ISN'T UPDATING DAILY!

Confirm that cron is being properly run on the site daily. Check the list at
Administer > Reports > Recent log entries. If cron doesn't seem to be running
daily, see Drupal's page about configuring cron jobs at:
http://drupal.org/cron

Also, keep in mind that at least 24 hours must elapse before Mollom Stats will
update its stats, so sometimes it will be longer. For example, if cron typically
runs on your site daily at 8:00 AM, and you install Mollom Stats or do something
else which causes the site's cache to be emptied at 10:00 AM on Monday, when
cron runs at 8:00 AM on Tuesday, the stats will not update since only 22 hours
will have passed. Only at 8:00 AM Wednesday will the stats update.